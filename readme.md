A simple keyboard driven git log viewer.
Similar to gitk but in a terminal based on urwid.

Advantages over `gitk`:

- same window
- no problems with resizing window
- window does not need to be as big
- keyboard navigation
- no separate configuration of colors
- more configurable (keyboard shortcuts, change the commands which produce the ouput)

Advantages over `git log`:

- interactively select a commit and look into it without needing to copy and paste a hash
- separators between different files in diff for a better overview
- easy copy of different informations via keyboard

This is work in progress.
I am using it already but some features are still missing.

Implemented are:

- display git log
- select a commit
- open a commit (display details including a diff)
- yank different information (`yy` for hash, `yt` for title, `yc` for committer, ...)
- display unstaged changes and index

Planned but not yet implemented are:

- search
- list of changed/added/deleted files
- config
- command line
- follow references
- side by side view of log and details
- help
- select branch by command line argument


# Installation
This program is written in Python 3 and is *not* compatible with Python 2.
It depends on the nonstandard library [urwid](https://pypi.org/project/urwid/) for the user interface which can be installed with `python3 -m pip install --user urwid` or via the system package manager.
This program is a wrapper around git therefore git needs to be installed.

Arch:

    $ sudo pacman -Syu
    $ sudo pacman -S python git
    $ python3 -m pip install --user urwid
    
    $ git clone 'https://gitlab.com/erzo/gitu.git'
    $ echo "alias gitu='`pwd`/gitu/main.py'" >> ~/.bashrc

Debian:

    $ sudo apt install python3 git
    $ python3 -m pip install --user urwid
    
    $ git clone 'https://gitlab.com/erzo/gitu.git'
    $ echo "alias gitu='`pwd`/gitu/main.py'" >> ~/.bashrc

Optional Dependencies:

- `wl-copy` (on Wayland) or `xclip` (on X) for copying to the clipboard


# Usage
Run `main.py` inside of the repository of which you want to display the history.
It displays a list of all commits on the current branch as well as well two lines for unstaged changes and the index at the very top (log view).
The top two lines are omitted if there are no (un)staged changes.

Select a commit with the down/up arrow keys or `j`/`k`.
Open a commit with the right arrow key, `l`, enter or space (details view).

Return to the log view with the left arrow key, `h` or escape.

Copy information about the currently selected commit to the clipboard from log view or details view (see Optional Dependencies) with:

- `yy` for hash
- `yt` for title
- `ya` for author
- `yc` for committer
- `yd` for commit date (ISO 8601-like)
- `yb` for commit body

Quit the program with `q`.
