#!/usr/bin/env python

import gettext
_ = gettext.gettext

import api_commands as commands


class quit(commands.Command):

	def execute(self, args):
		self.ui.quit()

class view(commands.Command):

	@classmethod
	def init_parser(cls, parser):
		modes = [
			cls.ui.VIEW_MODE_DETAILS,
			cls.ui.VIEW_MODE_LOG,
			cls.ui.VIEW_MODE_VER,
			cls.ui.VIEW_MODE_HOR,
		]
		parser.add_argument("mode", choices=modes)

	def execute(self, args):
		mode = args.mode
		self.ui.view(mode)


class yank(commands.Command):

	SELECTION_PRIMARY = "primary"
	SELECTION_CLIPBOARD = "clipboard"

	placeholders = {
		"id"   : "%H",
		"hash" : "%H",
		"short id"   : "%h",
		"short hash" : "%h",
		"title" : "%s",
		"body"  : "%b",
		"text"  : "%B",
		"author" : "%an <%ae>",
		"author name"  : "%an",
		"author email" : "%ae",
		"author date"  : "%ai",
		"committer" : "%cn <%ce>",
		"committer name"  : "%cn",
		"committer email" : "%ce",
		"committer date"  : "%ci",
	}

	@classmethod
	def init_parser(cls, parser):
		# Sort placeholders by length so that the longest possible match is replaced first.
		# Otherwise it might happen that the key "committer date" is interpreted
		# as the key "committer" followed by the suffix " date".
		cls.sorted_placeholders = [(key,val) for key,val in cls.placeholders.items()]
		cls.sorted_placeholders.sort(key=lambda o: len(o[0]), reverse=True)

		parser.add_argument("format", nargs='*', default='hash')
		parser.add_argument("--primary",   "-p", dest="selection", action="store_const", const=cls.SELECTION_PRIMARY, default=cls.SELECTION_CLIPBOARD)
		parser.add_argument("--clipboard", "-c", dest="selection", action="store_const", const=cls.SELECTION_PRIMARY)

	def execute(self, args):
		hash_id = self.ui.get_current_hash_id()
		if hash_id in (self.ui.ID_STAGED, self.ui.ID_UNSTAGED):
			self.ui.show_error(_("uncommitted changes have no meta info which could be copied"))
			return

		fmt = args.format
		fmt = " ".join(fmt)
		for human_readable, git_readable in self.sorted_placeholders:
			fmt = fmt.replace(human_readable, git_readable)

		cmd = ["git", "log", "-1", "--pretty=format:%s" % fmt, hash_id, "--"]
		out = self.run_and_get_output(cmd)

		selection = args.selection

		self.copy(out, selection)


	def copy(self, text, selection):
		if self.has_wlcopy():
			self.copy_wlcopy(text, selection)
		elif self.has_xsel():
			self.copy_xsel(text, selection)
		elif self.has_xclip():
			self.copy_xclip(text, selection)
		else:
			self.ui.show_error(_("please install wl-copy, xsel or xclip in order to use this feature"))
			return False

		self.ui.show_success(_("copied %r") % text)
		return True


	def copy_wlcopy(self, text, selection):
		cmd = ["wl-copy"]
		if selection == self.SELECTION_PRIMARY:
			cmd.append("--primary")
		cmd.append("--")
		cmd.append(text)

		self.run(cmd, check=True)

	def copy_xsel(self, text, selection):
		if selection == self.SELECTION_PRIMARY:
			selection = "--primary"
		else:
			selection = "--clipboard"

		cmd = ["xsel", "-i", selection]
		self.run(cmd, stdin=text, check=True)

	def copy_xclip(self, text, selection):
		if selection == self.SELECTION_PRIMARY:
			selection = "primary"
		else:
			selection = "clipboard"

		cmd = ["xclip", "-in", "-selection", selection]
		self.run(cmd, stdin=text, check=True)


	def has_wlcopy(self):
		cmd = ["wl-copy", "--version"]
		return self.run(cmd, check=False) == 0

	def has_xsel(self):
		cmd = ["xsel", "--version"]
		return self.run(cmd, check=False) == 0

	def has_xclip(self):
		cmd = ["xclip", "-version"]
		return self.run(cmd, check=False) == 0
