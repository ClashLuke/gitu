#!/usr/bin/env python

import subprocess


class Runner:

	encoding = "utf-8"

	RETURNCODE_PROGRAM_NOT_FOUND = "program-not-found"

	def run(self, cmd, stdin=None, check=True):
		"""
		Executes cmd without a shell and returns it's return code.
		Returns RETURNCODE_PROGRAM_NOT_FOUND if the program is not installed.

		cmd: list or tuple representing a command with it's arguments
		"""

		try:
			p = subprocess.run(cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, encoding=self.encoding, check=check, input=stdin)
			return p.returncode
		except FileNotFoundError:
			return self.RETURNCODE_PROGRAM_NOT_FOUND

	def run_and_get_output(self, cmd):
		"""
		Executes cmd without a shell and returns stdout as unicode/str.
		Returns RETURNCODE_PROGRAM_NOT_FOUND if the program is not installed.

		cmd: list or tuple representing a command with it's arguments
		"""

		p = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL, encoding=self.encoding, check=True)
		out = p.stdout
		return out


if __name__ == '__main__':
	r = Runner()
	rc = r.run(["git", "status"])
	if rc == Runner.RETURNCODE_PROGRAM_NOT_FOUND:
		print("git is not installed")
	elif rc != 0:
		print("current working directory is not in a git repository")
	else:
		commit = r.run_and_get_output(["git", "log", "-1", "--pretty=format:%H"])
		print("the last commit was %s" % commit)
