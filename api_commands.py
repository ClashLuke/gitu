#!/usr/bin/env python

import argparse
import gettext
_ = gettext.gettext

from api_subprocess import Runner


class ParseException(Exception):
	pass

class ArgumentParser(argparse.ArgumentParser):

	def error(self, message):
		raise ParseException(message)


class CommandContainer:

	SEP = " "

	def __init__(self, ui):
		self.commands = {}
		self.ui = ui

	def load_commands_from_module(self, module):
		for var in vars(module).values():
			try:
				if issubclass(var, Command) and var != Command:
					self.init_command_type(var)
					self.commands[var.get_name()] = var
			except TypeError:
				pass

	def init_command_type(self, cmd):
		cmd.ui = self.ui
		cmd.create_parser()

	def execute(self, command_line):
		if self.SEP in command_line:
			command_name, args = command_line.split(self.SEP, 1)
			args = args.split(self.SEP)
		else:
			command_name = command_line
			args = []

		cmd = self.commands.get(command_name)
		if cmd is None:
			self.error(_("no such command: %s") % command_name)
			return

		try:
			cmd(args)
		except ParseException as e:
			# error messages are internationalized
			# https://stackoverflow.com/a/35964548/2828064
			self.error(e)

	def error(self, msg):
		self.ui.show_error(msg)

class Command(Runner):
	"""Abstract command class"""

	name = None

	# ------- class methods -------

	@classmethod
	def get_name(cls):
		classdict = cls.__mro__[0].__dict__
		if 'name' in classdict and classdict['name']:
			return cls.name
		return cls.__name__

	@classmethod
	def create_parser(cls):
		cls.parser = ArgumentParser()
		cls.init_parser(cls.parser)

	@classmethod
	def init_parser(cls, parser):
		"""override this method if the command takes arguments"""
		pass


	# ------- instance methods -------

	def __init__(self, args):
		args = self.parser.parse_args(args)
		self.execute(args)

	def execute(self, args):
		"""
		override this method

		args: argparse.Namespace instance
		"""
		pass
