#!/usr/bin/env python3

# Copyright © 2020 erzo <erzo@posteo.de>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the copying.txt file for more details.

import gettext
_ = gettext.gettext

import urwid

import color_decoder
import urwid_constants
import urwid_color_encoder

import model
import api_commands


class LogEntryWidget(urwid.Text):

	def __init__(self, ln, hash_id=None):
		super().__init__(ln, wrap=LogTextLayout.WRAP_SPACE, align=LogTextLayout.ALIGN_INDENTATION)

		if hash_id is None:
			for fmt, text in ln:
				hash_id = model.LogModel.extract_hash_id(text)
				if hash_id is not None:
					break

		self.hash_id = hash_id

	def keypress(self, size, key):
		return key

	def selectable(self):
		return self.hash_id is not None


class LogTextLayout(urwid.StandardTextLayout):

	"""
	Extends the StandardTextLayout by a new align mode:
	ALIGN_INDENTATION

	In this mode text will be printed left aligned
	with the first line starting at the very left
	and the following lines indented.
	The indentation is specified by the first occurence
	of align_character in each printed line. This first
	occurence of align_character is hidden.
	Spaces directly following the align_character become
	part of the indentation as well because the %d place
	holder of git log expands to either nothing or some
	thing starting with " (" so that it is not possible
	to put the align_character after the space.
	"""

	ALIGN_LEFT = urwid.LEFT
	ALIGN_INDENTATION = "left indentation"

	WRAP_ANY   = urwid.ANY
	WRAP_SPACE = urwid.SPACE
	WRAP_CLIP  = urwid.CLIP

	def __init__(self, align_character):
		super().__init__()
		self.align_character = align_character
		self.len_align_character = len(align_character)

	def layout(self, text, width, align, wrap):
		if wrap == self.WRAP_CLIP and align == self.ALIGN_INDENTATION:
			align = self.ALIGN_LEFT

		if align == self.ALIGN_INDENTATION:
			return self.layout_left_indentation(text, width, wrap)
		else:
			return super().layout(text, width, align, wrap)

	def layout_left_indentation(self, text, width, wrap):
		align_character_i0 = text.find(self.align_character)

		if align_character_i0 == -1:
			return super().layout(text, width, self.ALIGN_LEFT, wrap)

		align_character_len = len(self.align_character)
		align_character_i1 = align_character_i0 + align_character_len

		n = len(text)
		indent = self.skip_space(text, align_character_i1, n)
		indent = indent - align_character_len

		if indent >= width:
			return super().layout(text, width, self.ALIGN_LEFT, wrap)

		layout_struct = []

		def append_first_line(i1):
			layout_struct.append([
				(align_character_i0, 0, align_character_i0),
				(i1 - align_character_i1, align_character_i1, i1)
			])

		if width >= n - align_character_len:
			append_first_line(n)
			return layout_struct

		if wrap == self.WRAP_SPACE:
			i1 = width
			i1 = self.rfind_space(text, align_character_i1, i1, i1)
			append_first_line(i1)

			line_width = width - indent
			while i1 < n:
				i = self.skip_space(text, i1, n)
				i1 = i + line_width
				if i1 >= n:
					i1 = n
				else:
					i1 = self.rfind_space(text, i, i1, i1)

				layout_struct.append([(indent, i), (i1-i, i, i1)])

		else: # wrap == self.WRAP_ANY:
			append_first_line(width)
			line_width = width - indent
			for i in range(width, n, line_width):
				if i + line_width > n:
					line_width = n - i
				layout_struct.append([(indent, i), (line_width, i, i + line_width)])

		return layout_struct

	@staticmethod
	def rfind_space(text, i, i1, alternative):
		assert i1 > i
		out = text.rfind(" ", i, i1)
		if out == -1:
			return alternative
		elif out == i:
			# without this I would run into an infinite loop
			return alternative
		else:
			return out

	@staticmethod
	def skip_space(text, i, n):
		# ensure that return value is valid index
		n -= 1

		while i < n and text[i] == ' ':
			i += 1

		return i


	def supports_align_mode(self, align):
		if align == self.ALIGN_INDENTATION:
			return True

		return super().supports_align_mode(align)

	def supports_wrap_mode(self, wrap):
		return super().supports_wrap_mode(wrap)

urwid.Text.layout = LogTextLayout("^")


class LogView(urwid.ListBox):

	_command_map = urwid.command_map.copy()
	_command_map['j'] = urwid.CURSOR_DOWN
	_command_map['k'] = urwid.CURSOR_UP

	def __init__(self, log_model):
		self._has_unstaged = False
		self._has_staged = False
		self.decoder = color_decoder.ColorDecoder()
		widgets = [self.create_line_widget(ln) for ln in log_model.get_lines()]
		body = urwid.SimpleFocusListWalker(widgets)
		super().__init__(body)

		#TODO: config
		#self.focus_staged()


	def create_line_widget(self, ln):
		hash_id = None
		if ln == model.ID_UNSTAGED:
			hash_id = ln
			ln = '[31m*[m ' + model.title_unstaged
			self._has_unstaged = True
		elif ln == model.ID_STAGED:
			hash_id = ln
			ln = '[32m*[m ' + model.title_staged
			self._has_staged = True

		ln = self.decoder.decode(ln)

		if not ln:
			ln = ""

		widget = LogEntryWidget(ln, hash_id=hash_id)
		widget = urwid.AttrMap(widget, None, focus_map=App.focus_map)
		return widget

	def get_current_hash_id(self):
		selected_log_entry, position = self.body.get_focus()

		if selected_log_entry is None:
			return None

		return selected_log_entry.base_widget.hash_id

	# ------- set focus -------

	def focus_unstaged(self):
		if not self._has_staged:
			return

		i = 0
		self.set_focus(i)

	def focus_staged(self):
		if not self._has_staged:
			return

		if self._has_unstaged:
			i = 1
		else:
			i = 0

		self.set_focus(i)

	def focus_latest_commit(self):
		i = self._has_unstaged + self._has_staged
		self.set_focus(i)


class DetailsView(urwid.ListBox):

	_command_map = urwid.command_map.copy()
	_command_map['j'] = urwid.CURSOR_DOWN
	_command_map['k'] = urwid.CURSOR_UP

	divider_widget = urwid.Divider("─")

	def __init__(self, details_model):
		self.hash_id = details_model.hash_id
		self.details_model = details_model

		self.decoder = color_decoder.ColorDecoder()
		self.widgets = []
		for ln in details_model.get_lines():
			self.append_line_widget(ln)
		body = urwid.SimpleFocusListWalker(self.widgets)
		super().__init__(body)

	def append_line_widget(self, ln):
		#TODO: ugly workaround
		# urwid can't leave handling of tabs to the terminal
		# otherwise it couldn't handle linebreaks appropriately.
		# Handling of tabs would belong in the TextLayout, though.
		ln = ln.replace("\t", "    ")

		ln = self.decoder.decode(ln)

		if not ln:
			ln = ""
		elif self.details_model.is_start_new_file(ln):
			self.widgets.append(self.divider_widget)

		widget = urwid.Text(ln, wrap=urwid.SPACE, align=urwid.LEFT)
		#widget = urwid.AttrMap(widget, None, focus_map=App.focus_map)
		self.widgets.append(widget)

class SubCommandMap:

	def __init__(self):
		self._command = dict()

	def __getitem__(self, key):
		return self._command.get(key)

	def __setitem__(self, key, value):
		self._command[key] = value


class App():

	CD = color_decoder.ColorDecoder
	COLOR = urwid_constants.COLOR
	FOCUSED = "_focused"

	ATTR_INFO  = "info"
	ATTR_ERROR = "error"
	ATTR_SUCCESS = "success"

	command_map_yank = SubCommandMap()
	command_map_yank['y'] = 'yank hash'
	command_map_yank['h'] = 'yank hash'
	command_map_yank['t'] = 'yank title'
	command_map_yank['a'] = 'yank author'
	command_map_yank['c'] = 'yank committer'
	command_map_yank['d'] = 'yank committer date'
	command_map_yank['b'] = 'yank body'

	command_map = urwid.CommandMap()
	command_map['q'] = 'quit'
	command_map['v'] = 'view ver'
	command_map['b'] = 'view hor'
	command_map['h'] = 'view log'
	command_map['l'] = 'view details'
	command_map['left']  = 'view log'
	command_map['esc']   = 'view log'
	command_map['right'] = 'view details'
	command_map['enter'] = 'view details'
	command_map[' ']     = 'view details'
	command_map['y']     = command_map_yank

	command_map_standard = command_map

	palette = [
		(ATTR_INFO,    COLOR.FG_DEFAULT, COLOR.BG_DEFAULT),
		(ATTR_ERROR,   COLOR.FG_RED,     COLOR.BG_DEFAULT),
		(ATTR_SUCCESS, COLOR.FG_GREEN,   COLOR.BG_DEFAULT),
	]
	focus_map = {}

	def __init__(self):
		g = urwid_color_encoder.Generator()
		self.palette += g.palette()
		self.focus_map.update(g.focus_map())

		self.log_view = LogView(model.LogModel())
		self.details_view = None
		self.footer = None #urwid.Text()
		self.frame_widget = urwid.Frame(self.log_view, footer=self.footer)
		self.commands = api_commands.CommandContainer(self)
		self.pressed_keys = ""

		import commands
		self.commands.load_commands_from_module(commands)

	def run(self):
		self.loop = urwid.MainLoop(self.frame_widget, self.palette,
			handle_mouse=False,
			unhandled_input=self.unhandled_input)
		self.loop.run()

	def unhandled_input(self, k):
		self.show_clear()
		cmd = self.command_map[k]
		if isinstance(cmd, SubCommandMap):
			self.append_key(k)
			self.show_info(self.pressed_keys)
			self.command_map = cmd
		elif cmd:
			self.reset_command_map()
			self.show_clear()
			self.commands.execute(cmd)
		else:
			self.append_key(k)
			self.show_error(_("undefined keyboard sortcut: %s") % self.pressed_keys)
			self.reset_command_map()

	KEY_SEP = _(", ")

	def append_key(self, key):
		if self.pressed_keys:
			self.pressed_keys += self.KEY_SEP
		self.pressed_keys += key

	def reset_command_map(self):
		self.pressed_keys = ""
		self.command_map = self.command_map_standard


	ID_STAGED = model.ID_STAGED
	ID_UNSTAGED = model.ID_UNSTAGED

	def get_current_hash_id(self):
		return self.log_view.get_current_hash_id()

	def quit(self):
		raise urwid.ExitMainLoop()


	# ------- view modes -------

	VIEW_MODE_DETAILS = "details"
	VIEW_MODE_LOG = "log"
	VIEW_MODE_VER = "ver"
	VIEW_MODE_HOR = "hor"

	def view(self, mode):
		if mode == self.VIEW_MODE_LOG:
			self.frame_widget.body = self.log_view
			return

		hash_id = self.get_current_hash_id()
		if self.details_view is None or self.details_view.hash_id != hash_id:
			details_model = model.DetailsModel(hash_id)
			self.details_view = DetailsView(details_model)

		if mode == self.VIEW_MODE_DETAILS:
			self.frame_widget.body = self.details_view
		else:
			#TODO
			self.show_info("not implemented %s" % mode)


	# ------- status bar -------

	def show_error(self, msg):
		if isinstance(msg, Exception):
			msg = str(msg)
		self.show(self.ATTR_ERROR, msg)

	def show_info(self, msg):
		self.show(self.ATTR_INFO, msg)

	def show_success(self, msg):
		self.show(self.ATTR_SUCCESS, msg)


	def show(self, attr, msg):
		self.frame_widget.footer = urwid.Text((attr,msg))

	def show_clear(self):
		self.frame_widget.footer = None


if __name__ == '__main__':
	a = App()
	a.run()
