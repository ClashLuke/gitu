#!/usr/bin/env python

import re
import gettext
_ = gettext.gettext

from api_subprocess import Runner

ID_UNSTAGED = "<unstaged>"
ID_STAGED = "<staged>"

title_unstaged = _('Local uncommitted changes, not checked in to index')
title_staged   = _('Local changes checked in to index but not committed')

color_title_unstaged = '[33m'
color_title_staged = color_title_unstaged
color_reset = '[m'
color_untracked =  '[34m'

class Model(Runner):

	cmd = None
	
	def get_text(self):
		return self.run_and_get_output(self.cmd)

	def get_lines(self):
		out = self.get_text()
		return out.splitlines()


class LogModel(Model):

	cmd_log = ["git", "log", "--color", "--graph", "--pretty=format:%Cred%h%Creset -^%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset", "--abbrev-commit"]
	cmd_status = ["git", "status", "--porcelain=v1"]

	re_hex = r'[0-9A-F]'
	reo_hash_id = re.compile(r"\b(%(re_hex)s{7,7}|%(re_hex)s{40,40})\b" % dict(re_hex=re_hex), re.IGNORECASE)

	def get_lines(self):
		self.cmd = self.cmd_log
		log = super().get_lines()

		self.cmd = self.cmd_status
		status = super().get_lines()

		unstaged = False
		staged = False

		# X: index
		# Y: work tree
		for ln in status:
			if ln[0] not in ' !?':
				staged = True
			if ln[1] not in ' !':
				unstaged = True

		if staged:
			log.insert(0, ID_STAGED)

		if unstaged:
			log.insert(0, ID_UNSTAGED)

		return log


	@classmethod
	def extract_hash_id(cls, text):
		m = cls.reo_hash_id.search(text)
		if m:
			return m.group(0)
		else:
			return None


class DetailsModel(Model):

	WC_HASH_ID = "{hash}"

	show_untracked = True

	cmd_pattern = ["git", "show", "--color", WC_HASH_ID]
	cmd_unstaged = ["git", "diff", "--color"]
	cmd_staged = cmd_unstaged + ["--cached"]
	cmd_untracked = ["git", "ls-files", "--others", "--exclude-standard"]

	start_new_file = r"diff --git "

	def __init__(self, hash_id):
		self.hash_id = hash_id
		if self.hash_id == ID_STAGED:
			self.cmd = self.cmd_staged
			self.title = color_title_staged + title_staged + color_reset
		elif self.hash_id == ID_UNSTAGED:
			self.cmd = self.cmd_unstaged
			self.title = color_title_unstaged + title_unstaged + color_reset
		else:
			self.cmd = [hash_id if x == self.WC_HASH_ID else x for x in self.cmd_pattern]
			self.title = None
	
	def get_lines(self):
		out = super().get_lines()

		if self.show_untracked and self.hash_id == ID_UNSTAGED:
			cmd = self.cmd
			self.cmd = self.cmd_untracked
			untracked = super().get_lines()
			self.cmd = cmd

			pattern = _("{color_untracked}untracked:{color_reset} {filename}")
			untracked = [pattern.format(filename=fn, color_untracked=color_untracked, color_reset=color_reset) for fn in untracked]
			if untracked:
				untracked.append("")

			out = untracked + out

		if self.title:
			out.insert(0, self.title)
			out.insert(1, "")
		return out

	def is_start_new_file(self, ln):
		"""ln: [(color, text), ...] as returned by color_decoder"""
		return ln[0][1].startswith(self.start_new_file)


if __name__ == '__main__':
	log = LogModel()
	for ln in log.get_lines():
		print(ln)
